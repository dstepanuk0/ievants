--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-09-12 12:24:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 59933)
-- Name: blocked_ips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blocked_ips (
    id bigint NOT NULL,
    address character varying(255),
    date date,
    reason character varying(255)
);


ALTER TABLE public.blocked_ips OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 59939)
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id bigint NOT NULL,
    mark_avg bigint,
    description character varying(255),
    name character varying(255) NOT NULL,
    type character varying(255),
    owner_id bigint,
    blocked boolean,
    photo_url character varying(255)
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 59945)
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 59947)
-- Name: file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file (
    id bigint NOT NULL,
    title character varying(255),
    url_file character varying(255)
);


ALTER TABLE public.file OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 59953)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 59955)
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profile (
    id bigint NOT NULL,
    accepted boolean,
    first_name character varying(25) NOT NULL,
    last_name character varying(25) NOT NULL,
    nickname character varying(25) NOT NULL,
    photo_url character varying(255),
    company_id bigint,
    user_id bigint
);


ALTER TABLE public.profile OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 59958)
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    id bigint NOT NULL,
    budget double precision,
    description character varying(255),
    employeemark bigint,
    employermark bigint,
    expirydate date,
    name character varying(255),
    photo_url character varying(255),
    status character varying(255),
    creator_id bigint,
    employee_id bigint,
    type_id bigint
);


ALTER TABLE public.project OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 60101)
-- Name: project_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_request (
    id bigint NOT NULL,
    accepted boolean,
    employee_id bigint,
    employer_id bigint,
    project_id bigint
);


ALTER TABLE public.project_request OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 59964)
-- Name: project_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_type (
    id bigint NOT NULL,
    name character varying(255),
    blocked boolean
);


ALTER TABLE public.project_type OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 59967)
-- Name: projectrequest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projectrequest (
    id bigint NOT NULL,
    employee_id bigint,
    employer_id bigint,
    project_id bigint
);


ALTER TABLE public.projectrequest OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 59970)
-- Name: projecttype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projecttype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projecttype_id_seq OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 59972)
-- Name: proposed_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposed_skills (
    company_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.proposed_skills OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 59975)
-- Name: required_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.required_skills (
    project_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.required_skills OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 59978)
-- Name: skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skills (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.skills OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 59981)
-- Name: skills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.skills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_id_seq OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 59983)
-- Name: user_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_info (
    id bigint NOT NULL,
    is_active boolean,
    password character varying(255),
    username character varying(255)
);


ALTER TABLE public.user_info OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 59989)
-- Name: user_recommendations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_recommendations (
    id bigint NOT NULL,
    valuation real,
    profile_id bigint,
    project_type_id bigint
);


ALTER TABLE public.user_recommendations OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 59992)
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    user_id bigint NOT NULL,
    roles character varying(255)
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- TOC entry 2914 (class 0 OID 59933)
-- Dependencies: 196
-- Data for Name: blocked_ips; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2915 (class 0 OID 59939)
-- Dependencies: 197
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (3, 4, '''''fvnfbvnbnfbnvbfbdfgvsdfvbnfdvbnfbndbnfvb', 'Polska12', 'EMPLOYEE', 14, false, 'project_stepanyk_gmail_com.jpg');
INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (1, 4, 'qqqwerg1111111111111111111111111111111111', 'Storona', 'EMPLOYER', 3, false, 'project_stepanyk_gmail_com.jpg');
INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (2, 5, '''''afvbdsfvbsdfnvbdnfvbbfnvbnfbvnfbnvbfnbv', 'EmploeeAnna', 'EMPLOYEE', 13, false, 'project_stepanyk_gmail_com.jpg');


--
-- TOC entry 2917 (class 0 OID 59947)
-- Dependencies: 199
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2919 (class 0 OID 59955)
-- Dependencies: 201
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.profile (id, accepted, first_name, last_name, nickname, photo_url, company_id, user_id) VALUES (3, true, 'Dmytro', 'Stepanyk', 'doromador', 'project_stepanyk_gmail_com.jpg', 1, 3);
INSERT INTO public.profile (id, accepted, first_name, last_name, nickname, photo_url, company_id, user_id) VALUES (13, true, 'Mike', 'Palahuta', 'palah', 'project_stepanyk_gmail_com.jpg', 3, 13);
INSERT INTO public.profile (id, accepted, first_name, last_name, nickname, photo_url, company_id, user_id) VALUES (137, true, 'Oksana', 'Oodchuk', 'ojksasasd', 'project_stepanyk_gmail_com.jpg', 1, 137);
INSERT INTO public.profile (id, accepted, first_name, last_name, nickname, photo_url, company_id, user_id) VALUES (14, false, 'Stepa', 'Savka', 'sava', 'project_stepanyk_gmail_com.jpg', 1, 14);
INSERT INTO public.profile (id, accepted, first_name, last_name, nickname, photo_url, company_id, user_id) VALUES (136, false, 'Oksana', 'Odochuk', 'oskaosodchuk', 'project_stepanyk_gmail_com.jpg', 1, 136);


--
-- TOC entry 2920 (class 0 OID 59958)
-- Dependencies: 202
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (116, 34534, 'dsf asasg areg adr gadfg ', NULL, NULL, '2019-09-11', 'Record4', 'PROJECT_stepanyk_gmail_comECEDB.jpg', 'NEW', 1, NULL, 2);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (59, 2132, 'gfht6yjj tyhjtyrjut yrrtyhrty', NULL, NULL, '2019-09-28', 'LoLaa', 'project_CABCA_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (124, 23534, 'set aert sdrg sdg sd', NULL, NULL, '2019-12-28', 'Record21', 'PROJECT_stepanyk_gmail_comDBABB.jpg', 'NEW', 1, NULL, 2);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (113, 242322, 'desf awet awt ', NULL, NULL, '2019-11-23', 'Record2', 'PROJECT_stepanyk_gmail_comCEACA.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (32, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Amazon', 'project_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (29, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Bomba', 'project_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (109, 3453, 'sdef sasdgf adrg ', NULL, NULL, '2019-12-21', 'Dima Stepanuk', 'project_BCBCA_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (39, 432400, 'Your hair will be too amazing. dfddddddddddddddddddddddddddddd', NULL, NULL, '2020-02-29', 'Head&Sholders', 'project_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (79, 234234, 'description', NULL, NULL, '2019-08-31', 'Name d', 'project_BBECD_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (58, 123213, 'dsrftrdsgdsg rrsyhrty ht ', NULL, NULL, '2019-08-31', 'Opapa', 'project_ABDBE_stepanyk_gmail_com.jpg', 'IN_PROGRESS', 1, 3, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (26, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Lacalut', 'project_stepanyk_gmail_com.jpg', 'IN_PROGRESS', 1, 2, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (141, 23422, 'sdfsdfs sdfsd sdfsd sdfsdf sdfsdf sdfsdfs sdfsdf sdfsdfs sdfsdfs sdfsdfs sddfsfs sdfsfsd sdfsfs sdfsdfs', NULL, NULL, '2019-09-21', 'Lviv2000', 'PROJECT_stepanyk_gmail_comDEDAE.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (61, 324234, ' ret wert qert ert ', NULL, NULL, '2019-08-31', 'qewrewrwrtew rtw t', 'project_DEBCD_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (117, 24342, 'asfr wawert aewrt farestgf', NULL, NULL, '2019-09-11', 'Record5', 'PROJECT_stepanyk_gmail_comCCEEC.jpg', 'NEW', 1, NULL, 2);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (114, 345453, 'asfasw aertg aserg ', NULL, NULL, '2019-11-23', 'Record3', 'PROJECT_stepanyk_gmail_comCEAEB.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (45, 341230, 'The best project around world.', NULL, NULL, '2021-04-23', 'Google', 'project_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 1);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (43, 24324, 'The first project ', NULL, NULL, '2020-01-23', 'First', 'project_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (125, 43543, 'sfg sdg sdfhssfth sf', NULL, NULL, '2019-11-09', 'Record323', 'PROJECT_stepanyk_gmail_comCCADB.jpg', 'NEW', 1, NULL, 2);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (135, 1111, 'gfdfgdfgdfggfd', NULL, NULL, '2019-09-21', 'OKSANAdasdfas', 'PROJECT_stepanyk_gmail_comBBEAE.jpg', 'NEW', 1, NULL, 5);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (126, 23423, 'sdfsadfas asdf asf ', NULL, NULL, '2019-11-23', 'Record90', 'PROJECT_stepanyk_gmail_comDEECD.jpg', 'NEW', 1, NULL, 2);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (103, 23423, 'sf dsasdf asdf asf ', NULL, NULL, '2019-10-19', 'First New Project', 'project_BDACE_stepanyk_gmail_com.jpg', 'NEW', 1, NULL, 10);


--
-- TOC entry 2931 (class 0 OID 60101)
-- Dependencies: 213
-- Data for Name: project_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (78, NULL, 3, 1, 61);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (87, NULL, 3, 1, 58);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (106, NULL, 3, 1, 59);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (109, NULL, 2, 1, 61);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (111, NULL, 2, 1, 26);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (112, NULL, 2, 1, 58);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (114, NULL, 2, 1, 103);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (122, NULL, 3, 1, 116);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (127, NULL, 3, 1, 125);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (139, NULL, 3, 1, 114);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (140, NULL, 3, 1, 117);
INSERT INTO public.project_request (id, accepted, employee_id, employer_id, project_id) VALUES (142, NULL, 3, 1, 124);


--
-- TOC entry 2921 (class 0 OID 59964)
-- Dependencies: 203
-- Data for Name: project_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project_type (id, name, blocked) VALUES (4, NULL, NULL);
INSERT INTO public.project_type (id, name, blocked) VALUES (5, NULL, NULL);
INSERT INTO public.project_type (id, name, blocked) VALUES (10, 'Java', false);
INSERT INTO public.project_type (id, name, blocked) VALUES (2, 'dev', false);
INSERT INTO public.project_type (id, name, blocked) VALUES (1, 'Dev', false);


--
-- TOC entry 2922 (class 0 OID 59967)
-- Dependencies: 204
-- Data for Name: projectrequest; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.projectrequest (id, employee_id, employer_id, project_id) VALUES (2, 2, 1, 32);
INSERT INTO public.projectrequest (id, employee_id, employer_id, project_id) VALUES (4, 3, 1, 39);
INSERT INTO public.projectrequest (id, employee_id, employer_id, project_id) VALUES (5, 2, 1, 45);
INSERT INTO public.projectrequest (id, employee_id, employer_id, project_id) VALUES (48, 3, 1, 26);


--
-- TOC entry 2924 (class 0 OID 59972)
-- Dependencies: 206
-- Data for Name: proposed_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.proposed_skills (company_id, skill_id) VALUES (3, 5);
INSERT INTO public.proposed_skills (company_id, skill_id) VALUES (2, 7);


--
-- TOC entry 2925 (class 0 OID 59975)
-- Dependencies: 207
-- Data for Name: required_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 7);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 9);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 10);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 11);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 12);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 13);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (116, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 14);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (141, 15);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (117, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (124, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (58, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (125, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (126, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (79, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (45, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (109, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (113, 5);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (114, 5);


--
-- TOC entry 2926 (class 0 OID 59978)
-- Dependencies: 208
-- Data for Name: skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.skills (id, name) VALUES (5, 'Spring 58');
INSERT INTO public.skills (id, name) VALUES (7, 'FFFF');
INSERT INTO public.skills (id, name) VALUES (9, 'Java');
INSERT INTO public.skills (id, name) VALUES (10, 'C#');
INSERT INTO public.skills (id, name) VALUES (11, 'Hibernate');
INSERT INTO public.skills (id, name) VALUES (12, 'SQL');
INSERT INTO public.skills (id, name) VALUES (13, 'GIT');
INSERT INTO public.skills (id, name) VALUES (14, 'C++');
INSERT INTO public.skills (id, name) VALUES (15, 'Python');


--
-- TOC entry 2928 (class 0 OID 59983)
-- Dependencies: 210
-- Data for Name: user_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_info (id, is_active, password, username) VALUES (1, false, '$2a$10$MZ29ZamSVrPfs4h5oGjBT.JUPmb5/EV6u5I8emSu0hSX4rnLqI/Se', 'dstepanuk0@gmail.c0m');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (2, true, '$2a$10$6lsNzPWDK4J8J2f9IBCJQOVSi53riPQDPPLGXh6Z3skVV8HgjYWN6', 'dstepanuk0@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (3, true, '$2a$10$WEBoWVlOl74k34xQUveCk.G9lwlkuif9IHDgSCxB89we76eLH3zdC', 'stepanyk@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (13, true, '$2a$10$buFjltxQ34xpamsHnAbZReqGckKUYDuc8GsNbdiSZgmIJ.ylqyf2u', 'palahuta@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (14, true, '$2a$10$FIK/RfosPNi/Sgi8j/SRqOr032ddm.PVL0JF8e8uToxBfgsrUflgW', 'stepa@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (44, false, '$2a$10$1cHHcAfMTcJVIJpoeF4MKeFnbiqa11xiU/mJWCNkQPbyB1uwxI/Hu', 'employee1@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (136, true, '$2a$10$I9sea7BLvlZH6rceD4msDuM4f/pgKNoRXzyLhPNVnHJXilBEbHX/6', 'admin@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (137, true, '$2a$10$URSJ2i1xxUb7VO9IWF9CNezAT1ROzRMdl5ev3xoElH2NXBLPgBpqi', 'admin1@gmail.com');


--
-- TOC entry 2929 (class 0 OID 59989)
-- Dependencies: 211
-- Data for Name: user_recommendations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_recommendations (id, valuation, profile_id, project_type_id) VALUES (38, 7, 3, 10);
INSERT INTO public.user_recommendations (id, valuation, profile_id, project_type_id) VALUES (42, 7.5, 13, 10);
INSERT INTO public.user_recommendations (id, valuation, profile_id, project_type_id) VALUES (51, 8, 13, 1);
INSERT INTO public.user_recommendations (id, valuation, profile_id, project_type_id) VALUES (123, 6, 13, 2);


--
-- TOC entry 2930 (class 0 OID 59992)
-- Dependencies: 212
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_role (user_id, roles) VALUES (1, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (2, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (3, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (14, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (44, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (13, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (136, 'ADMIN');
INSERT INTO public.user_role (user_id, roles) VALUES (137, 'ADMIN');


--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 198
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_id_seq', 3, true);


--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 200
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 142, true);


--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 205
-- Name: projecttype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projecttype_id_seq', 5, true);


--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 209
-- Name: skills_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skills_id_seq', 15, true);


--
-- TOC entry 2749 (class 2606 OID 59996)
-- Name: blocked_ips blocked_ips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blocked_ips
    ADD CONSTRAINT blocked_ips_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 59998)
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 60000)
-- Name: file file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 60002)
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 60004)
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- TOC entry 2773 (class 2606 OID 60105)
-- Name: project_request project_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_request
    ADD CONSTRAINT project_request_pkey PRIMARY KEY (id);


--
-- TOC entry 2759 (class 2606 OID 60006)
-- Name: project_type project_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_type
    ADD CONSTRAINT project_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2761 (class 2606 OID 60008)
-- Name: projectrequest projectrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT projectrequest_pkey PRIMARY KEY (id);


--
-- TOC entry 2763 (class 2606 OID 60010)
-- Name: proposed_skills proposed_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT proposed_skills_pkey PRIMARY KEY (company_id, skill_id);


--
-- TOC entry 2765 (class 2606 OID 60012)
-- Name: required_skills required_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT required_skills_pkey PRIMARY KEY (project_id, skill_id);


--
-- TOC entry 2767 (class 2606 OID 60014)
-- Name: skills skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- TOC entry 2769 (class 2606 OID 60018)
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (id);


--
-- TOC entry 2771 (class 2606 OID 60020)
-- Name: user_recommendations user_recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT user_recommendations_pkey PRIMARY KEY (id);


--
-- TOC entry 2774 (class 2606 OID 60021)
-- Name: companies fk2rnygfpf0ca7252gep0gd0itq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT fk2rnygfpf0ca7252gep0gd0itq FOREIGN KEY (owner_id) REFERENCES public.profile(id);


--
-- TOC entry 2787 (class 2606 OID 60026)
-- Name: user_recommendations fk3ujn1vowvpmtqgx3l2y5olb1a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT fk3ujn1vowvpmtqgx3l2y5olb1a FOREIGN KEY (profile_id) REFERENCES public.profile(id);


--
-- TOC entry 2792 (class 2606 OID 60116)
-- Name: project_request fk49eh03i303y7t5fmi2kswt2b6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_request
    ADD CONSTRAINT fk49eh03i303y7t5fmi2kswt2b6 FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2775 (class 2606 OID 60031)
-- Name: profile fk4f2icjx0bevus365xaioo14gx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk4f2icjx0bevus365xaioo14gx FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 2785 (class 2606 OID 60036)
-- Name: required_skills fk4inem3msersanj1bkgp7b1hgs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fk4inem3msersanj1bkgp7b1hgs FOREIGN KEY (skill_id) REFERENCES public.skills(id);


--
-- TOC entry 2791 (class 2606 OID 60111)
-- Name: project_request fk7bur1t36fvfher7mf6ghjhrnf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_request
    ADD CONSTRAINT fk7bur1t36fvfher7mf6ghjhrnf FOREIGN KEY (employer_id) REFERENCES public.companies(id);


--
-- TOC entry 2776 (class 2606 OID 60041)
-- Name: profile fk9c6o0i2wom50761484uhrqni6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk9c6o0i2wom50761484uhrqni6 FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2780 (class 2606 OID 60046)
-- Name: projectrequest fkdofbm71dweqlrf9oq0yulpl55; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkdofbm71dweqlrf9oq0yulpl55 FOREIGN KEY (employer_id) REFERENCES public.companies(id);


--
-- TOC entry 2777 (class 2606 OID 60051)
-- Name: project fkeaohcvks7ix2a3vbjj9mvaghx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkeaohcvks7ix2a3vbjj9mvaghx FOREIGN KEY (type_id) REFERENCES public.project_type(id);


--
-- TOC entry 2786 (class 2606 OID 60056)
-- Name: required_skills fkfu1g6rg1luupmuuj7xnxi6dcu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fkfu1g6rg1luupmuuj7xnxi6dcu FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2778 (class 2606 OID 60061)
-- Name: project fkfwq11dxwrexdea7sg3dttyq84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkfwq11dxwrexdea7sg3dttyq84 FOREIGN KEY (employee_id) REFERENCES public.companies(id);


--
-- TOC entry 2779 (class 2606 OID 60066)
-- Name: project fkgrfaybenjkhnefl8y9sf96p4p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkgrfaybenjkhnefl8y9sf96p4p FOREIGN KEY (creator_id) REFERENCES public.companies(id);


--
-- TOC entry 2781 (class 2606 OID 60071)
-- Name: projectrequest fkipj7o2yktf43muqeydh4vndsy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkipj7o2yktf43muqeydh4vndsy FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2788 (class 2606 OID 60076)
-- Name: user_recommendations fkjp8ey7w6vc3jhqo1u1023pbwt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT fkjp8ey7w6vc3jhqo1u1023pbwt FOREIGN KEY (project_type_id) REFERENCES public.project_type(id);


--
-- TOC entry 2789 (class 2606 OID 60081)
-- Name: user_role fkm90yi1ak9h9tbct25k3km0hia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkm90yi1ak9h9tbct25k3km0hia FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2790 (class 2606 OID 60106)
-- Name: project_request fkn76wm2q7jt7hkyjoncfjq8kt9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_request
    ADD CONSTRAINT fkn76wm2q7jt7hkyjoncfjq8kt9 FOREIGN KEY (employee_id) REFERENCES public.companies(id);


--
-- TOC entry 2783 (class 2606 OID 60086)
-- Name: proposed_skills fknwq7xv28skpddav5kpfvyxo64; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fknwq7xv28skpddav5kpfvyxo64 FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 2784 (class 2606 OID 60091)
-- Name: proposed_skills fkoga7edw0sv4c4lwo1wpefh7l2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fkoga7edw0sv4c4lwo1wpefh7l2 FOREIGN KEY (skill_id) REFERENCES public.skills(id);


--
-- TOC entry 2782 (class 2606 OID 60096)
-- Name: projectrequest fkq2xjyl0x4i374kk9d02ag4c3j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkq2xjyl0x4i374kk9d02ag4c3j FOREIGN KEY (employee_id) REFERENCES public.companies(id);


-- Completed on 2019-09-12 12:24:39

--
-- PostgreSQL database dump complete
--

