package connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {


    public static Connection getConnection() {
        try {
        ConnectionProperties conection = new ConnectionProperties();
        String DRIVER;
        String URL;
        String USERNAME;
        String PASSWORD;

            DRIVER = conection.getConectionData().get("DRIVER");
            URL = conection.getConectionData().get("URL");
            USERNAME = conection.getConectionData().get("USERNAME");
            PASSWORD = conection.getConectionData().get("PASSWORD");

            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USERNAME, PASSWORD);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found");
        } catch (SQLException ex) {

            throw new RuntimeException("Error connecting to the database", ex);

        }
     catch (IOException e) {
        e.printStackTrace();
    }

        return null;
    }
    public static void main(String[] args) throws Exception {
       ConnectionDB connectionDB = new ConnectionDB();
       connectionDB.getConnection();
    }
}



