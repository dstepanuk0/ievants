package connection;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConnectionProperties {

    public Map<String,String> getConectionData() throws IOException {
        InputStream inputStream = ConnectionProperties.class.getClassLoader().getResourceAsStream("conection.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        Map<String,String> conection = new HashMap();

        conection.put("DRIVER",properties.getProperty("driver"));
        conection.put("URL",properties.getProperty("url"));
        conection.put("USERNAME",properties.getProperty("username"));
        conection.put("PASSWORD",properties.getProperty("password"));

        return conection;
    }


}
