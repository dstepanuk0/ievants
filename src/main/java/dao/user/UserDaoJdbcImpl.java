package dao.user;

import connection.ConnectionDB;
import model.Role;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoJdbcImpl implements IUserDao {
    public UserDaoJdbcImpl() {
        //default constructor
    }


    public  User insertUser(User user ) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "INSERT INTO public.\"user\" (\"first_name\", lastname, role, password, email, contact, active,photo)  VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1,user.getFirstname());
        statement.setString(2,user.getLastname());
        statement.setString(3, user.getRole().name());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getEmail());
        statement.setString(6, user.getContact());
        statement.setBoolean(7, user.isActive());
        statement.setString(8, user.getPhoto());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }


    public User getUser(int id) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        User user = new User();
        String query = "SELECT * FROM \"user\" WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        user.setId(resultSet.getInt("id"));
        user.setFirstname( resultSet.getString("first_name"));
        user.setLastname( resultSet.getString("lastname"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setContact(resultSet.getString("contact"));
        user.setRole(Role.valueOf(resultSet.getString("role")));
        user.setActive(resultSet.getBoolean("active"));
        user.setPhoto(resultSet.getString("photo"));
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }


    public User updateUser(User user) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "UPDATE \"user\" SET first_name = ?,lastname =?, email = ?, password = ?, role = ?, active = ?,contact = ?,photo = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, user.getFirstname());
        statement.setString(2, user.getLastname());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getRole().name());
        statement.setBoolean(6, user.isActive());
        statement.setString(7, user.getContact());
        statement.setString(8, user.getPhoto());
        statement.setInt(9, user.getId());

        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }


    public List<User> getAll() throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM \"user\" WHERE role != 'ADMIN' ORDER BY id ASC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            users.add(new User(
                    resultSet.getInt("id"),
                    resultSet.getString("first_name"),
                    resultSet.getString("lastname"),
                    resultSet.getString("email"),
                    resultSet.getString("contact"),
                    resultSet.getString("password"),
                    Role.valueOf(resultSet.getString("role")),
                    resultSet.getBoolean("active"),
                    resultSet.getString("photo")
            ));
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return users;
    }

    public User getUserByEmail(String email) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        User user = new User();
        String query = "SELECT * FROM \"user\" WHERE email=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, email);
        ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            user.setId(resultSet.getInt("id"));
            user.setFirstname(resultSet.getString(2));
            user.setLastname(resultSet.getString("lastname"));
            user.setEmail(resultSet.getString("email"));
            user.setContact(resultSet.getString("contact"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            user.setActive(resultSet.getBoolean("active"));
            user.setPhoto(resultSet.getString("photo"));
            resultSet.close();
            statement.close();
            dataStorageJDBC.getConnection().close();
            return user;
        }
        else
            return null;
    }


    public void banUser(int userId) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "UPDATE \"user\" SET active = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setInt(2, userId);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }


    public void unBanUser(int userId) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "UPDATE \"user\" SET active = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setInt(2, userId);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

}
