package dao.post;
import model.Post;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public interface IPostDao {
    Post insertPost(Post post) throws SQLException;
    void deletePost(int id) throws SQLException;
    List<Post> getAll() throws SQLException;
    Post getPostByDate(Timestamp date1) throws SQLException;

    List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException;
    List<Post> getPostsByTag(String  tagUnit) throws SQLException;
}
