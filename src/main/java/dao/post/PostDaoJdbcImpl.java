package dao.post;

import connection.ConnectionDB;
import dao.tag.TagDaoJdbcImpl;
import model.Post;
import model.Role;
import model.User;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PostDaoJdbcImpl implements IPostDao {

    public PostDaoJdbcImpl() {
    }

    @Override
    public Post insertPost(Post post) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        LocalDateTime datetime = LocalDateTime.now();
        post.setDateOfPublication(Timestamp.valueOf(datetime));
        String query = "INSERT INTO post (name, description, photo,date_of_publication,id_user,city,country) VALUES (?,?,?,?,?,?,?)";
        query.replace('T',' ');
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1,post.getName());
        statement.setString(2,post.getDescription());
        statement.setString(3,post.getImageURL());
        statement.setTimestamp(4, Timestamp.valueOf(datetime));
        statement.setInt(5,post.getAuthor().getId());
        statement.setString(6,post.getCity());
        statement.setString(7,post.getCountry());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return post;
    }

    @Override
    public Post getPostByDate(Timestamp date) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        Post post = new Post();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON id_user = \"user\".id  WHERE date_of_publication=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setTimestamp(1, date);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        post.setId(resultSet.getInt("id"));
        post.setName(resultSet.getString("name"));
        post.setDescription(resultSet.getString("description"));
        post.setCity(resultSet.getString("city"));
        post.setCountry(resultSet.getString("country"));

        post.setAuthor(new User(
                resultSet.getInt("id"),
                resultSet.getString("first_name"),
                resultSet.getString("lastname"),
                resultSet.getString("email"),
                resultSet.getString("contact"),
                resultSet.getString("password"),
                Role.valueOf(resultSet.getString("role")),
                resultSet.getBoolean("active"),
                resultSet.getString("photo")));
        return post;
    }

    @Override
    public void deletePost(int id) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query2 = "DELETE FROM tag_post WHERE id_post=?";
        PreparedStatement statement2 = dataStorageJDBC.getConnection().prepareStatement(query2);
        statement2.setInt(1, id);
        statement2.executeUpdate();

        String query = "DELETE FROM post WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

    @Override
    public List<Post> getAll() throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON user_id = \"user\".id";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Timestamp dateOfPublication;
            dateOfPublication = resultSet.getTimestamp("date_of_publication");

            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setName(resultSet.getString("name"))
                    .setDescription(resultSet.getString("description"))
                    .setDateOfPublication(dateOfPublication)
                    .setCity(resultSet.getString("city"))
                    .setCountry(resultSet.getString("country"))
                    .setAuthor(new User(resultSet.getInt("id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("email"),
                            resultSet.getString("contact"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active"),
                            resultSet.getString("photo")
                    ))
                   .build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }


    @Override
    public List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON id_user = \"user\".id WHERE date_of_publication IS NOT NULL AND id_user = " + authorId + " ORDER BY date_of_publication DESC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();

            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setName(resultSet.getString("name"))
                    .setDescription(resultSet.getString("description"))
                    .setDateOfPublication(resultSet.getTimestamp("date_of_publication"))
                    .setImageURL(resultSet.getString("photo"))
                    .setCity(resultSet.getString("city"))
                    .setCountry(resultSet.getString("country"))
                    .setAuthor(new User(resultSet.getInt("id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("email"),
                            resultSet.getString("contact"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active"),
                            resultSet.getString("photo")
                            ))
                    .build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }

    @Override
    public List<Post> getPostsByTag(String  tagUnit) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post JOIN tag_post ON post.id=tag_post.id_post JOIN " +
                "tag ON tag.id=tag_post.id_tag JOIN \"user\" ON \"user\".id = post.id_user WHERE tag.tag=?  ORDER BY date_of_publication DESC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, tagUnit);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {

            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setName(resultSet.getString("name"))
                    .setDescription(resultSet.getString("description"))
                    .setDateOfPublication(resultSet.getTimestamp("date_of_publication"))
                    .setImageURL(resultSet.getString("photo"))
                    .setCity(resultSet.getString("city"))
                    .setCountry(resultSet.getString("country"))
                    .setAuthor(new User(resultSet.getInt("id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("email"),
                            resultSet.getString("contact"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active"),
                            resultSet.getString("photo")
                    ))
                    .build()
            );
        }
        return posts;
    }
}
