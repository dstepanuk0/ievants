package dao.tag;

import connection.ConnectionDB;
import model.Post;
import model.Tag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagDaoJdbcImpl implements ITagDao {

    public TagDaoJdbcImpl() {
    }

    @Override
    public Tag insertTag(Tag tag) throws SQLException {

        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "INSERT INTO tag (tag) VALUES (?)";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, tag.getTag());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tag;
    }

    public void insertTagPost(int postId,  List<Tag> tags) throws SQLException {

        ConnectionDB dataStorageJDBC = new ConnectionDB();
        String query = "INSERT INTO tag_post (id_post,id_tag) VALUES (?,?)";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        for (Tag tag : tags) {
            statement.setInt(1, postId);
            statement.setInt(2, tag.getId());
            statement.executeUpdate();
        }
        statement.close();
        dataStorageJDBC.getConnection().close();

    }

    @Override
    public Tag getTagByTag(String tagUnit) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        Tag tag = new Tag();
        String query = "SELECT * FROM tag WHERE tag=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, tagUnit);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        tag.setId(resultSet.getInt("id"));
        tag.setTag(resultSet.getString("tag"));
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tag;
    }


    @Override
    public List<Tag> checkTag(Tag tagUnit) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        Tag tag = new Tag();
        int count = 0;
        List<Tag> tags = new ArrayList<>();
        String query = "SELECT * FROM tag";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            if (resultSet.getString("tag").equals(tagUnit.getTag()))
                count++;

        }
        if(count > 0)
            tags.add(getTagByTag(tagUnit.getTag()));
        if(count == 0)
        {
            insertTag(tagUnit);
            tags.add(getTagByTag(tagUnit.getTag()));
        }

        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tags;
    }

    @Override
    public List<Tag> getTagsByPost(List<Post> posts) throws SQLException {
        ConnectionDB dataStorageJDBC = new ConnectionDB();
        List<Tag> tags = new ArrayList<>();
        String query = "SELECT * FROM tag join tag_post on tag.id=tag_post.id_tag join " +
                "post on post.id=tag_post.id_post WHERE post.id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        for(Post post : posts) {

            statement.setInt(1, post.getId());
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Tag tag = new Tag();
                tag.setId(post.getId());
                tag.setTag(resultSet.getString("tag"));
                  tags.add(tag);

            }
        }
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tags;
    }


}
