package dao.tag;

import model.Post;
import model.Tag;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface ITagDao {

    Tag insertTag(Tag tag) throws SQLException;
    List<Tag> checkTag(Tag tagUnit) throws SQLException;
    Tag getTagByTag(String tagUnit) throws SQLException;
    void insertTagPost(int postId,  List<Tag> tags) throws SQLException;
    List<Tag> getTagsByPost(List<Post> posts) throws SQLException;
}
