package servlet;

import org.apache.commons.codec.digest.DigestUtils;

import service.user.UserServiceImpl;

import model.Role;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "SignUpServlet", urlPatterns = "/signup")
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserServiceImpl userService = new UserServiceImpl();
        User user = new User();
        List<String> errorMessages = new ArrayList<>();
        user.setRole(Role.USER);
        user.setActive(true);
        user.setFirstname(req.getParameter("firstname"));
        user.setLastname(req.getParameter("lastname"));

        user.setContact(req.getParameter("contact"));

        //Check password verification
        if(!req.getParameter("password").equals(req.getParameter("passwordConfirmation")))
            errorMessages.add("Password confirmation does not match");

        //Check email verification
        try {
            if(userService.isEmail(req.getParameter("email")))
                errorMessages.add("The email is already registered");
            if(!userService.formatEmail(req.getParameter("email")))
                errorMessages.add("The email is not the correct format");
            //validate user
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //there aren't any errors - insert user and go to login page with message about success of registration
        if(errorMessages.isEmpty()) {
            try {
                user.setPassword(DigestUtils.md5Hex(req.getParameter("password")));
                user.setEmail(req.getParameter("email"));
                user.setPhoto("http://www.cb.cz/benatky/images/avatar.png");
                userService.insertUser(user);
            } catch (SQLException e) {

             // resp.sendRedirect(req.getContextPath()+"/error?type=general");
            }
        }
        //there are some errors - go to sign up page with validation error messages
        else {
            req.setAttribute("errors", errorMessages);
            req.getRequestDispatcher("notauthorized/signup.jsp").forward(req, resp);
        }

        req.setAttribute("successSignUp", "Your account has been successfully created!");
        req.getRequestDispatcher("notauthorized/signin.jsp").forward(req, resp);
        return;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("notauthorized/signup.jsp").forward(req, resp);
    }


}

