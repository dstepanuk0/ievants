package servlet;

import model.User;
import service.post.PostServiceImpl;
import service.tag.TagServiceImpl;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserServlet", urlPatterns = "/user")
public class UserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      //  request.getRequestDispatcher("authorized/user.jsp").forward(request, response);

        HttpSession session = request.getSession();
        User user  = (User) session.getAttribute("user");
        request.setAttribute("user", user);
        TagServiceImpl tagService = new TagServiceImpl();
        PostServiceImpl postService = new PostServiceImpl();
        try {
           request.setAttribute("tags",tagService.getTagsByPost(postService.getPublishedPostsByAuthor(user.getId())));
         //   System.out.println(tagService.getTagsByPost(postService.getPublishedPostsByAuthor(user.getId())).get(4).getTag());

           request.setAttribute("posts",postService.getPublishedPostsByAuthor(user.getId()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("authorized/user.jsp").forward(request, response);

    }
}
