package servlet;

import model.User;
import org.apache.commons.codec.digest.DigestUtils;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "EditInfServlet", urlPatterns = "/editInf")
public class EditInfServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        List<String> errorMessages = new ArrayList<>();
        HttpSession session = request.getSession();
        try {
            User user  = (User) session.getAttribute("user");
            if(request.getParameter("email") != "")
            {
                if (userService.isEmail(request.getParameter("email")))
                    errorMessages.add("The email is already registered");
                if (!userService.formatEmail(request.getParameter("email")))
                    errorMessages.add("The email is not the correct format");
            }
            if(errorMessages.isEmpty()) {

                if(request.getParameter("email") != "")
                    user.setEmail(request.getParameter("email"));
                if(request.getParameter("contact") != "")
                    user.setContact(request.getParameter("contact"));

                userService.updateUser(user);
                session.setAttribute("user", user);
                response.sendRedirect(request.getContextPath() + "/user");
            }
            //there are some errors - go to sign up page with validation error messages
            else {
                request.setAttribute("errors", errorMessages);
                request.getRequestDispatcher("authorized/editInf.jsp").forward(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserServiceImpl userService = new UserServiceImpl();
        try {
          User  user = userService.getUser(Integer.parseInt(request.getParameter("userId")));
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            request.getRequestDispatcher("authorized/editInf.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
