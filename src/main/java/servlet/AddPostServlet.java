package servlet;

import model.Post;
import model.Tag;
import model.User;
import service.post.PostServiceImpl;
import service.tag.TagServiceImpl;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AddPostServlet", urlPatterns = "/addPost")
public class AddPostServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Tag> tagsforTable = new ArrayList<>();
        HttpSession session = request.getSession();
        User user  = (User) session.getAttribute("user");
        PostServiceImpl postService = new PostServiceImpl();
        Tag tag = new Tag();
        TagServiceImpl tagService = new TagServiceImpl();
        Post post = new Post();
        post.setName( request.getParameter("name"));
        post.setCity( request.getParameter("city"));
        post.setCountry( request.getParameter("country"));
        post.setDescription( request.getParameter("description"));
        post.setAuthor(user);
        post.setImageURL( request.getParameter("photo"));
        try {
             post = postService.insertPost(post);

            String[] tags = tagService.splitTag( request.getParameter("tag"));
            for(int i = 1;i<tags.length;i++) {
                tag.setTag(tags[i]);
                tagsforTable.add(tagService.checkTag(tag).get(0));
            }
             post = postService.getPostByDate(post.getDateOfPublication());
            tagService.insertTagPost(post.getId(),tagsforTable);
            session.setAttribute("user", user);
            response.sendRedirect(request.getContextPath() + "/user");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserServiceImpl userService = new UserServiceImpl();
        try {
            User user = userService.getUser(Integer.parseInt(request.getParameter("userId")));
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            userService.updateUser(user);
            request.getRequestDispatcher("authorized/addPost.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
