package servlet;

import model.User;
import service.post.PostServiceImpl;
import service.tag.TagServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "GetPostsByTagServlet", urlPatterns = "/getPosts")
public class GetPostsByTagServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();


        TagServiceImpl tagService = new TagServiceImpl();
        PostServiceImpl postService = new PostServiceImpl();
        try {
            request.getParameter("tag");
            request.setAttribute("posts",postService.getPostsByTag(request.getParameter("tag")));

            request.setAttribute("tags",tagService.getTagsByPost(postService.getPostsByTag(request.getParameter("tag"))));
            request.getRequestDispatcher("authorized/postsByTag.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    }

