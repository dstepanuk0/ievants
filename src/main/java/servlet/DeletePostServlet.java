package servlet;

import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeletePostServlet", urlPatterns = "/delete")
public class DeletePostServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();

        try {
            postService.deletePost(Integer.parseInt(request.getParameter("postId")));
            response.sendRedirect(request.getContextPath() + "/user");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
