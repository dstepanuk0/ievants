package servlet;


import model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet("/signin")
public class SignInServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(SignInServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        String email = request.getParameter("email");
        String password = DigestUtils.md5Hex(request.getParameter("password"));


        //Check email and password for the content
        if (email.isEmpty() || password.isEmpty()) {
            request.setAttribute("errors", "Please input all fields!");
            request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
        } else {
            try {
                //Check email for the format
                if (!userService.formatEmail(request.getParameter("email"))) {
                    request.setAttribute("errors", "The email is not the correct format");
                    request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
                }

                User user = userService.getUserByEmail(email);

                if (user != null) {
                    if (user.isActive()) {
                        if (!user.getPassword().equals(password)) {
                            request.setAttribute("errors", "You entered the wrong password");
                            request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
                        }

                        if (user.getRole().name() == "ADMIN") {
                            HttpSession session = request.getSession();
                            session.setAttribute("user", user);
                            response.sendRedirect(request.getContextPath() + "/users");
                            return;
                        }

                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        response.sendRedirect(request.getContextPath() + "/user");
                        return;
                    } else
                        request.getRequestDispatcher("notauthorized/ban.jsp").forward(request, response);
                } else {
                    request.setAttribute("errors", "Incorrect password or email!");
                    request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
                response.sendRedirect(request.getContextPath() + "/error?type=general");
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
    }
}