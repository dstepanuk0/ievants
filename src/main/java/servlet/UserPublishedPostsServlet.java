package servlet;


import model.Post;
import model.User;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/posts")
public class UserPublishedPostsServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(UserPublishedPostsServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PostServiceImpl postService = new PostServiceImpl();
            User user = (User)request.getSession().getAttribute("user");
            List<Post> publishedPostByAuthor = postService.getPublishedPostsByAuthor(user.getId());
            request.setAttribute("publishedPostByAuthor", publishedPostByAuthor);
            request.getRequestDispatcher("authorized/user-posts.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath()+"/error?type=general");
        }
    }
}
