package servlet;

import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "BanServlet", urlPatterns = "/ban")
public class BanServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        try {
            if (request.getParameter("active").equals("true")) {
                userService.banUser(Integer.parseInt(request.getParameter("userId")));
                response.sendRedirect(request.getContextPath() + "/users");
            }
            if (request.getParameter("active").equals("false")) {
                userService.unBanUser(Integer.parseInt(request.getParameter("userId")));
                response.sendRedirect(request.getContextPath() + "/users");
            }
        } catch (SQLException e) {

            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }
}
