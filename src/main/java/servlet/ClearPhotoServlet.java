package servlet;

import model.User;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ClearPhotoServlet", urlPatterns = "/clearPhoto")
public class ClearPhotoServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserServiceImpl userService = new UserServiceImpl();
        try {
            User user = userService.getUser(Integer.parseInt(request.getParameter("userId")));
            user.setPhoto("");
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            userService.updateUser(user);
            response.sendRedirect(request.getContextPath() + "/user");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
