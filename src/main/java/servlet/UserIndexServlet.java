package servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import dao.user.UserDaoJdbcImpl;
import model.Post;
import model.User;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "UsersServlet", urlPatterns = "/users")
public class UserIndexServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();

        try {

            request.setAttribute("users", userService.getAll());
            request.getRequestDispatcher("authorized/user-index.jsp").forward(request, response);
        } catch (SQLException e) {

            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }

    }
}
