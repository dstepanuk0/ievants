package servlet;

import model.User;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "EditPhotoServlet", urlPatterns = "/editPhoto")
public class EditPhotoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        HttpSession session = request.getSession();
        if(request.getParameter("photo") =="")
        {
            request.setAttribute("errors", "You have not entered any value");
            request.getRequestDispatcher("authorized/editPhoto.jsp").forward(request, response);
        }
        else
        {
            try {
                User user  = (User) session.getAttribute("user");
                user.setPhoto(request.getParameter("photo"));
                userService.updateUser(user);
                session.setAttribute("user", user);
                response.sendRedirect(request.getContextPath() + "/user");
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserServiceImpl userService = new UserServiceImpl();
        try {
            User  user = userService.getUser(Integer.parseInt(request.getParameter("userId")));
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            request.getRequestDispatcher("authorized/editPhoto.jsp").forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    }
