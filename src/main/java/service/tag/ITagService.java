package service.tag;

import model.Post;
import model.Tag;

import java.sql.SQLException;
import java.util.List;

public interface ITagService {
    String[] splitTag(String tag) ;
    List<Tag> checkTag(Tag tag) throws SQLException;
    void insertTagPost( int postId, List<Tag> tags) throws SQLException;
    List<Tag> getTagsByPost(List<Post> posts) throws SQLException;

}
