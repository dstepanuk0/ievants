package service.tag;

import dao.tag.TagDaoJdbcImpl;
import model.Post;
import model.Tag;
import java.sql.SQLException;
import java.util.List;

public class TagServiceImpl implements ITagService{

    private TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();


    @Override
    public String[] splitTag(String tag) {
        String[] tags= tag.split("#");

        return tags;
    }

    @Override
    public List<Tag> getTagsByPost(List<Post> posts) throws SQLException {
     return tagDaoJdbc.getTagsByPost(posts);
    }

    @Override
    public void insertTagPost( int postId, List<Tag> tags) throws SQLException {
       tagDaoJdbc.insertTagPost(postId,tags);
    }

    @Override
    public List<Tag> checkTag(Tag tag) throws SQLException {
        return tagDaoJdbc.checkTag(tag);
    }




}
