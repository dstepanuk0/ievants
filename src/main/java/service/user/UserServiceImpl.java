package service.user;
import dao.user.UserDaoJdbcImpl;
import model.User;
import java.sql.SQLException;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class UserServiceImpl implements IUserService {

    private UserDaoJdbcImpl userDaoJdbc = new UserDaoJdbcImpl();

    public UserServiceImpl(){}
    @Override
    public User insertUser(User user) throws SQLException {

       return userDaoJdbc.insertUser(user);
    }

    @Override
    public User getUser(int id) throws SQLException {
        return userDaoJdbc.getUser(id);
    }

    @Override
    public User updateUser(User user) throws SQLException {
        return userDaoJdbc.updateUser(user);
    }

    @Override
    public List<User> getAll() throws SQLException {
        return userDaoJdbc.getAll();
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        return userDaoJdbc.getUserByEmail(email);
    }

    public boolean isEmail(String email) throws SQLException {
        return userDaoJdbc.getUserByEmail(email) != null;
    }

    public boolean formatEmail(String email)
    {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{1,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
       return matcher.matches();
    }

    @Override
    public void banUser(int userId) throws SQLException {
        userDaoJdbc.banUser(userId);
    }

    @Override
    public void unBanUser(int userId) throws SQLException {
        userDaoJdbc.unBanUser(userId);
    }

}
