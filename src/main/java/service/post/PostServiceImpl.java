package service.post;
import dao.post.PostDaoJdbcImpl;
import model.Post;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class PostServiceImpl implements IPostService  {

    private PostDaoJdbcImpl postDaoJdbc = new PostDaoJdbcImpl();
    @Override
    public Post insertPost(Post post ) throws SQLException {
        return postDaoJdbc.insertPost(post);
    }


    public Post getPostByDate(Timestamp date) throws SQLException
    {
        return postDaoJdbc.getPostByDate(date);
    }


    @Override
    public void deletePost(int id) throws SQLException {
        postDaoJdbc.deletePost(id);
    }

    @Override
    public List<Post> getAll() throws SQLException {
        return postDaoJdbc.getAll();
    }



    @Override
    public List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException {
        return postDaoJdbc.getPublishedPostsByAuthor(authorId);
    }

    @Override
    public List<Post> getPostsByTag(String  tagUnit) throws SQLException {
            return postDaoJdbc.getPostsByTag(tagUnit);
    }
}

