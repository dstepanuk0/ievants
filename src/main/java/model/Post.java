
package model;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.validation.constraints.NotEmpty;
public class Post {
    private int id;

    @NotEmpty(message = "Title can not to be empty")
    private String name;
    @NotEmpty(message = "Description can not to be empty")
    private String description;
    @NotNull(message = "Choose image")
    private String imageURL;
    private Timestamp dateOfPublication;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String country;
    private String city;
    private User author;


    public Post() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Timestamp getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(Timestamp dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getDateFormatted() {
        String s = new SimpleDateFormat("MM/dd/yyyy HH:mm").format(dateOfPublication);
        return s;
    }

    public static class Builder {
        private int id;
        private String name;
        private String description;
        private String imageURL;
        private Timestamp dateOfPublication;
        private String city;

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        private String country;
        private User author;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setImageURL(String imageURL) {
            this.imageURL = imageURL;
            return this;
        }

        public Builder setDateOfPublication(Timestamp dateOfPublication) {
            this.dateOfPublication = dateOfPublication;
            return this;
        }

        public Builder setAuthor(User author) {
            this.author = author;
            return this;
        }

        public Post build() {
            Post post = new Post();
            post.id = id;
            post.name = name;
            post.description = description;
            post.imageURL = imageURL;
            post.dateOfPublication = dateOfPublication;
            post.author = author;
            post.country = country;
            post.city = city;
            return post;
        }
    }
}