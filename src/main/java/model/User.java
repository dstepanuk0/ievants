package model;

import org.apache.commons.codec.digest.DigestUtils;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class User {
    private int id;


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @NotNull(message = "User name can not be null")
    @Size(min = 2, message = "User name must contain a minimum of 2 characters")
    private String firstname;


    @Size(min = 1, message = "User name must contain a minimum of 1 characters")
    private String lastname;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @NotNull(message = "Email can not be null")
    @Size(min = 9, message = "Email must contain a minimum of 9 characters")
    @Email(message = "Illegal email")
    private String email;

    @Size(min = 10, message = "Contact must contain a minimum of 10 characters")
    private String contact;

    @NotNull(message = "Password can not be null")
    @Size(min = 8, message = "Password must contain a minimum of 8 characters")
    private String password;
    private Role role;
    private boolean active;

    private String photo;

    public User() {
    }

    public User(int id, String firstname,String lastname, String email,String contact, String password, Role role, boolean active,String photo) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.contact = contact;
        this.password = password;
        this.role = role;
        this.active = active;
        if(photo == "")
            this.photo = "http://www.cb.cz/benatky/images/avatar.png";
        else
             this.photo = photo;
    }

    private User(User target) {
        if (target != null) {
            this.id = target.id;
            this.firstname = target.firstname;
            this.lastname = target.lastname;
            this.email = target.email;
            this.contact = target.contact;
            this.password = target.password;
            this.role = target.role;
            this.active = target.active;
            this.photo = target.photo;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {

        if(photo == "")
            this.photo = "http://www.cb.cz/benatky/images/avatar.png";
        else
            this.photo = photo;
    }

    public User clone() {
        return new User(this);
    }

}
