<%--
  Created by IntelliJ IDEA.
  User: dsteptc
  Date: 25.06.2019
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" >BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item active"><a href="${pageContext.request.contextPath}/signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="${pageContext.request.contextPath}/signin"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="container">
    <div class="row centered-reg-aut-form">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign Up</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="${pageContext.request.contextPath}/signup" id="registerForm">
                        <br><br><br>
                        <input required type="text" name="firstname" id="fistname" class="form-control" placeholder="First User Name"><br>
                        <input required type="text" name="lastname" id="lastname" class="form-control" placeholder="Last User Name"><br>
                        <input required type="email" name="email" id="email" class="form-control" placeholder="Email"><br>
                        <input required type="tel" name="contact" id="contact" class="form-control" placeholder="Contact"><br>
                        <input required type="password" name="password" id="password" class="form-control" placeholder="Password"><br>
                        <input required type="password" name="passwordConfirmation" id="passwordConfirmation" class="form-control" placeholder="Confirm Password"><br>
                            <input type="submit" value="Sign Up" class="btn btn-success col-md-4 col-md-offset-4" >
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="error-message">
                        ${requestScope.errors}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
