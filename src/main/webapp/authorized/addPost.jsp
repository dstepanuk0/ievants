<%--
  Created by IntelliJ IDEA.
  User: dsteptc
  Date: 03.07.2019
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Post</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" >BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span
                        class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                </li>

                <li class="nav-item active"><a href="${pageContext.request.contextPath}/addPost"><i
                        class="fas fa-plus"></i> Add Post</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="row add-post-form">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Post</h3>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/addPost" id="addPostForm">
                <div class="panel-body">
                    <br><br><br>
                    <input required type="text" name="name" id="name" class="form-control" placeholder="Name"><br>
                    <input required type="text" name="tag" id="tag" class="form-control" placeholder="Tag"><br>
                    <input required  name="city" id="city" class="form-control" placeholder="City"><br>
                    <input required  name="country" id="country" class="form-control" placeholder="Country"><br>
                    <textarea required name="description" form="addPostForm"
                              placeholder="Enter post description here..."
                              class="form-control resize-none"></textarea><br>
                    <div class="form-group">
                        <label class="control-label">Input url for yor photo</label>
                        <input required type="text" name="photo" id="photo" class="form-control"
                               placeholder="Url your photo"><br>
                    </div>
                    <br>
                    <input type="submit" value="Add" class="btn btn-success col-md-4 col-md-offset-4" >
                </div>
            </form>
            <div class="panel-footer">
                <div class="success-message">
                    ${requestScope.successAdd}
                </div>
                <div class="error-message">
                    ${requestScope.errors}
                </div>
            </div>
        </div>
    </div>
</div>
<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>
