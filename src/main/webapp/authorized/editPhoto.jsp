<%--
  Created by IntelliJ IDEA.
  User: dsteptc
  Date: 02.07.2019
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand">BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span
                        class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                </li>
                <li><a href="${pageContext.request.contextPath}/addpost"><i class="fas fa-plus"></i> Add</a></li>
            </ul>
        </div>
    </nav>
</header>


<div class="container">
    <br><br><br>
    <div class="row centered-reg-aut-form">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title" >Input url for yor photo</h2>
                </div>
                <div class="panel-body">
                    <form method="post" action="${pageContext.request.contextPath}/editPhoto" id="editPhoto">
                        <br><br><br>

                        <input   name="photo" id="photo" class="form-control" placeholder="Photo"><br>
                        <input type="submit" value="Сhange" class="btn btn-success col-md-4 col-md-offset-4" >
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="error-message">
                        ${requestScope.errors}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
