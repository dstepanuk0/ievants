<%--
  Created by IntelliJ IDEA.
  User: dsteptc
  Date: 04.07.2019
  Time: 20:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">


</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand">BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span
                        class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>

        </div>
    </nav>
</header>

<%-------------------------------------------------------------------------------------------------------------------------------%>
<style type="text/css">
    .block1 {
        width: 345px;
        height: 600px;
        background: #ccc;
        padding: 5px;
        padding-right: 5px;
        border: ridge 1px black;
        float: left;
        margin-left: 1%;
    }

    .btn {
        color: #fff;
        background: #363636;
    }

    #blog {
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .blog-post {
        margin-bottom: 120px;

    }
    .blog-post-title {
        text-transform: uppercase;
        letter-spacing: 1px;
        font-size: 30px;
        font-weight: 900;
        color: #1c1c1c;
        font-family: 'Montserrat',sans-serif;
    }
    .blog-post-info {
        list-style: none;
        padding: 0;
        margin-top: 10px;
    }
    .blog-post-info li {
        display: inline-block;
        font-size: 13px;
        margin-right: 15px;
        color: #888;
        font-family: 'Montserrat',sans-serif;
    }
    .blog-post-info li i {
        font-size: 13px;
        margin-right: 3px;
    }

    .blog-post-date {
        width: 80px;
    }
    .blog-post-date h1{
        margin: 0;
        font-family: 'Montserrat',sans-serif;
        font-weight: 900;
    }
    .blog-post-date h5 {
        margin: 0;
        font-family: 'Montserrat',sans-serif;
        font-weight: 700;
    }


    .blog-post p {
        color: #888;
        font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
        font-size: 18px;
    }

    .blog-post .blog-img{
        margin-bottom: 15px;
    }
    .blog-post .img-responsive {
        width: 50%;
    }


</style>

<%-------------------------------------------------------------------------------------------------------------------------------%>


<br><br><br>


<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div  class="alert alert-primary" role="alert">
                    <c:forEach items="${requestScope.posts}" var="post">
                        <div class="blog-post">
                            <br><br><br>
                            <h3 class="blog-post-title">${post.name}</h3>
                            <div class="blog-post-date">
                                <p>${post.getDateFormatted()}</p>

                            </div>
                            <h4>${post.city} ${post.country}</h4>
                            <img src=${post.imageURL} alt="Avatar" height="300" width="300"
                                 class=" blog-img  img-circle">

                            <p>${post.description}<p>
                            <p style="color:#008080"  ><c:forEach items="${requestScope.tags}" var="tag">
                                <c:if test = "${tag.id == post.id}">

                                    <a  href="${pageContext.request.contextPath}/getPosts?tag=${tag.tag}">#<c:out value = "${tag.tag}"/></a>

                                </c:if>

                            </c:forEach>
                            </p>
                            <br>
                            <h2>${post.author.firstname}  ${post.author.lastname}</h2>
                        </div>

                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</section>


<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>

