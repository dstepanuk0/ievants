--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-07-17 19:46:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 34849)
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    id bigint NOT NULL,
    mark_avg bigint,
    description character varying(255),
    name character varying(255),
    type character varying(255),
    description2 character varying(255)
);


ALTER TABLE public.company OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 34843)
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 34838)
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 34855)
-- Name: owned_companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.owned_companies (
    profile_id bigint NOT NULL,
    company_id bigint NOT NULL
);


ALTER TABLE public.owned_companies OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 34858)
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profile (
    id bigint NOT NULL,
    first_name character varying(255),
    is_root boolean,
    last_name character varying(255),
    nickname character varying(255),
    photo_url character varying(255),
    company_id bigint,
    user_id bigint
);


ALTER TABLE public.profile OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 34864)
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    id bigint NOT NULL,
    budget double precision,
    description character varying(255),
    employeemark bigint,
    employermark bigint,
    expirydate date,
    name character varying(255),
    status character varying(255),
    creator_id bigint,
    employee_id bigint,
    type_id bigint
);


ALTER TABLE public.project OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 34870)
-- Name: project_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_type (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.project_type OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 34873)
-- Name: proposed_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposed_skills (
    company_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.proposed_skills OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 34876)
-- Name: required_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.required_skills (
    project_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.required_skills OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 34879)
-- Name: skill; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skill (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.skill OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 34882)
-- Name: statement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.statement (
    id bigint NOT NULL,
    text character varying(255),
    employee_id bigint,
    employer_id bigint,
    project_id bigint
);


ALTER TABLE public.statement OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 34885)
-- Name: user_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_info (
    id bigint NOT NULL,
    password character varying(255),
    username character varying(255)
);


ALTER TABLE public.user_info OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 34891)
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    user_id bigint NOT NULL,
    roles character varying(255)
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- TOC entry 2898 (class 0 OID 34849)
-- Dependencies: 198
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2897 (class 0 OID 34843)
-- Dependencies: 197
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-1', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.607031', 1, 'EXECUTED', '8:96a9572d902435fe70db7f9735efa27c', 'createTable tableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-2', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.611729', 2, 'EXECUTED', '8:73542d7cbf6a21bc2e6491c3c3ec3086', 'createTable tableName=owned_companies', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-3', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.6204', 3, 'EXECUTED', '8:ea62ece1d0a3e3c279462354f99e6d8e', 'createTable tableName=profile', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-4', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.628075', 4, 'EXECUTED', '8:ab497a031f6b2d2367643bb82175f00a', 'createTable tableName=project', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-5', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.631799', 5, 'EXECUTED', '8:3ee02a263433e96ca6d3320f5fa2f981', 'createTable tableName=project_type', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-6', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.636305', 6, 'EXECUTED', '8:d3e1c290edc9dc6c6c56910ac13601d8', 'createTable tableName=proposed_skills', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-7', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.640639', 7, 'EXECUTED', '8:d6938550b3ff0ce5db96731164d2460e', 'createTable tableName=required_skills', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-8', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.643536', 8, 'EXECUTED', '8:04c3a6d5e4221d52182b209ca556837a', 'createTable tableName=skill', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-9', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.647242', 9, 'EXECUTED', '8:fed6bb0aa3e0562c162052737b730ec4', 'createTable tableName=statement', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-10', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.656054', 10, 'EXECUTED', '8:3e837708da2237f34a59a86e09701c37', 'createTable tableName=user_info', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-11', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.659647', 11, 'EXECUTED', '8:ce06497344dabfb1927eb6eda9170d3a', 'createTable tableName=user_role', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-12', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.66632', 12, 'EXECUTED', '8:6e0717eb43f84bfcaad255ca7179a7b1', 'addPrimaryKey constraintName=company_pkey, tableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-13', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.672404', 13, 'EXECUTED', '8:784ad87a898c195bbf589dff8c1aa6ed', 'addPrimaryKey constraintName=owned_companies_pkey, tableName=owned_companies', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-14', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.679784', 14, 'EXECUTED', '8:26ae92b8cd4eff932200838caf3378fc', 'addPrimaryKey constraintName=profile_pkey, tableName=profile', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-15', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.687012', 15, 'EXECUTED', '8:f6fab29a9cb20ca067a943076c766f83', 'addPrimaryKey constraintName=project_pkey, tableName=project', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-16', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.693201', 16, 'EXECUTED', '8:c9a584dbeef7fca5cd48076573fd2275', 'addPrimaryKey constraintName=project_type_pkey, tableName=project_type', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-17', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.699859', 17, 'EXECUTED', '8:642cafa9d4ed5a6bab8843239cd0b960', 'addPrimaryKey constraintName=proposed_skills_pkey, tableName=proposed_skills', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-18', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.706528', 18, 'EXECUTED', '8:c98be6dff7d8eeae6ce9e157bb212dab', 'addPrimaryKey constraintName=required_skills_pkey, tableName=required_skills', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-19', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.71253', 19, 'EXECUTED', '8:f22f332d0fe28bcf635c068213df36ae', 'addPrimaryKey constraintName=skill_pkey, tableName=skill', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-20', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.718619', 20, 'EXECUTED', '8:1f3e0508fff84e5318c9a5061e6f4df5', 'addPrimaryKey constraintName=statement_pkey, tableName=statement', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-21', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.725567', 21, 'EXECUTED', '8:925312975e41adea4a1f18f6a0c61f1e', 'addPrimaryKey constraintName=user_info_pkey, tableName=user_info', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-22', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.732865', 22, 'EXECUTED', '8:9ceba38e1a93604f3c542ac2d8761004', 'addUniqueConstraint constraintName=uk_k39llf316u1hgrb5a5johryge, tableName=owned_companies', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-23', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.736164', 23, 'EXECUTED', '8:0b72964622407a5f04c9737440a570dd', 'addForeignKeyConstraint baseTableName=statement, constraintName=fk3lr6y3r0ftdukruej6o1r2acw, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-24', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.738423', 24, 'EXECUTED', '8:06c430c68dd8571f1a450c1dc397c5f9', 'addForeignKeyConstraint baseTableName=project, constraintName=fk64ih6kll0jdynef0hkiuhmwl7, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-25', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.740526', 25, 'EXECUTED', '8:69d72f28b9cc0d15c2c02341e62e5706', 'addForeignKeyConstraint baseTableName=project, constraintName=fk6yb3o88i4mmvji7dfi8bc7xdo, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-26', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.743494', 26, 'EXECUTED', '8:645d24e18c4699637a4b4adcb3fd273d', 'addForeignKeyConstraint baseTableName=profile, constraintName=fk9c6o0i2wom50761484uhrqni6, referencedTableName=user_info', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-27', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.745782', 27, 'EXECUTED', '8:abbe2ea482673b7a90f43d8cad5e692c', 'addForeignKeyConstraint baseTableName=required_skills, constraintName=fkafb2b2ithgw59h6dqc25b0t8d, referencedTableName=skill', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-28', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.747872', 28, 'EXECUTED', '8:09ff50f1fc91afca475af1fe98863980', 'addForeignKeyConstraint baseTableName=profile, constraintName=fkcgtsav2kgqnq61ejgtsn85fgo, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-29', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.7503', 29, 'EXECUTED', '8:d6feaa6f04636adaa9692727330d4256', 'addForeignKeyConstraint baseTableName=project, constraintName=fkeaohcvks7ix2a3vbjj9mvaghx, referencedTableName=project_type', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-30', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.752635', 30, 'EXECUTED', '8:51637de8b4430306b6fa69255e248434', 'addForeignKeyConstraint baseTableName=required_skills, constraintName=fkfu1g6rg1luupmuuj7xnxi6dcu, referencedTableName=project', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-31', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.754897', 31, 'EXECUTED', '8:d9da62fe1c4ed565efd715d49788a264', 'addForeignKeyConstraint baseTableName=proposed_skills, constraintName=fkhsarj7qgjgjmr5hsdlq8f38fn, referencedTableName=skill', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-32', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.757485', 32, 'EXECUTED', '8:71c62becec420a20c7b197cc64412ff5', 'addForeignKeyConstraint baseTableName=owned_companies, constraintName=fkil8sxyscorxq53kgc7imvho4t, referencedTableName=profile', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-33', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.76075', 33, 'EXECUTED', '8:ade18fb632be464298d5ae52e703fd4c', 'addForeignKeyConstraint baseTableName=owned_companies, constraintName=fkjgmoyc5xjswq0v4qb4ne9udf3, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-34', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.762696', 34, 'EXECUTED', '8:22b1a8b1fece850639b466694b49fdb4', 'addForeignKeyConstraint baseTableName=user_role, constraintName=fkm90yi1ak9h9tbct25k3km0hia, referencedTableName=user_info', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-35', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.764815', 35, 'EXECUTED', '8:03a5f76f667862d24729b70a13b2d766', 'addForeignKeyConstraint baseTableName=statement, constraintName=fkosqftck2cd6g6cqjjvtx9ofho, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-36', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.767018', 36, 'EXECUTED', '8:ce4487a29b956ed1b02e0cd7a5c87be3', 'addForeignKeyConstraint baseTableName=statement, constraintName=fkpoaihwcxx9fqcnmw1ikl2ft5j, referencedTableName=project', '', NULL, '3.6.1', NULL, NULL, '3380510567');
INSERT INTO public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) VALUES ('1563353074866-37', 'dsteptc (generated)', 'db.change.log.generatedNew1.xml', '2019-07-17 19:21:50.769041', 37, 'EXECUTED', '8:f2c12975dc2cc1f33d36ff502da7a659', 'addForeignKeyConstraint baseTableName=proposed_skills, constraintName=fksx4ibndx5e6em79nuc0nc1593, referencedTableName=company', '', NULL, '3.6.1', NULL, NULL, '3380510567');


--
-- TOC entry 2896 (class 0 OID 34838)
-- Dependencies: 196
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.databasechangeloglock (id, locked, lockgranted, lockedby) VALUES (1, false, NULL, NULL);


--
-- TOC entry 2899 (class 0 OID 34855)
-- Dependencies: 199
-- Data for Name: owned_companies; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2900 (class 0 OID 34858)
-- Dependencies: 200
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2901 (class 0 OID 34864)
-- Dependencies: 201
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2902 (class 0 OID 34870)
-- Dependencies: 202
-- Data for Name: project_type; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2903 (class 0 OID 34873)
-- Dependencies: 203
-- Data for Name: proposed_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2904 (class 0 OID 34876)
-- Dependencies: 204
-- Data for Name: required_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2905 (class 0 OID 34879)
-- Dependencies: 205
-- Data for Name: skill; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2906 (class 0 OID 34882)
-- Dependencies: 206
-- Data for Name: statement; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2907 (class 0 OID 34885)
-- Dependencies: 207
-- Data for Name: user_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_info (id, password, username) VALUES (1, 'dima', 'kolia');
INSERT INTO public.user_info (id, password, username) VALUES (2, 'vasya', 'chornyy');


--
-- TOC entry 2908 (class 0 OID 34891)
-- Dependencies: 208
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2739 (class 2606 OID 34895)
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- TOC entry 2737 (class 2606 OID 34842)
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- TOC entry 2741 (class 2606 OID 34897)
-- Name: owned_companies owned_companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.owned_companies
    ADD CONSTRAINT owned_companies_pkey PRIMARY KEY (profile_id, company_id);


--
-- TOC entry 2745 (class 2606 OID 34899)
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- TOC entry 2747 (class 2606 OID 34901)
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 34903)
-- Name: project_type project_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_type
    ADD CONSTRAINT project_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 34905)
-- Name: proposed_skills proposed_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT proposed_skills_pkey PRIMARY KEY (company_id, skill_id);


--
-- TOC entry 2753 (class 2606 OID 34907)
-- Name: required_skills required_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT required_skills_pkey PRIMARY KEY (project_id, skill_id);


--
-- TOC entry 2755 (class 2606 OID 34909)
-- Name: skill skill_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT skill_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 34911)
-- Name: statement statement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statement
    ADD CONSTRAINT statement_pkey PRIMARY KEY (id);


--
-- TOC entry 2743 (class 2606 OID 34915)
-- Name: owned_companies uk_k39llf316u1hgrb5a5johryge; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.owned_companies
    ADD CONSTRAINT uk_k39llf316u1hgrb5a5johryge UNIQUE (company_id);


--
-- TOC entry 2759 (class 2606 OID 34913)
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (id);


--
-- TOC entry 2771 (class 2606 OID 34916)
-- Name: statement fk3lr6y3r0ftdukruej6o1r2acw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statement
    ADD CONSTRAINT fk3lr6y3r0ftdukruej6o1r2acw FOREIGN KEY (employer_id) REFERENCES public.company(id);


--
-- TOC entry 2764 (class 2606 OID 34921)
-- Name: project fk64ih6kll0jdynef0hkiuhmwl7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fk64ih6kll0jdynef0hkiuhmwl7 FOREIGN KEY (employee_id) REFERENCES public.company(id);


--
-- TOC entry 2765 (class 2606 OID 34926)
-- Name: project fk6yb3o88i4mmvji7dfi8bc7xdo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fk6yb3o88i4mmvji7dfi8bc7xdo FOREIGN KEY (creator_id) REFERENCES public.company(id);


--
-- TOC entry 2762 (class 2606 OID 34931)
-- Name: profile fk9c6o0i2wom50761484uhrqni6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk9c6o0i2wom50761484uhrqni6 FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2769 (class 2606 OID 34936)
-- Name: required_skills fkafb2b2ithgw59h6dqc25b0t8d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fkafb2b2ithgw59h6dqc25b0t8d FOREIGN KEY (skill_id) REFERENCES public.skill(id);


--
-- TOC entry 2763 (class 2606 OID 34941)
-- Name: profile fkcgtsav2kgqnq61ejgtsn85fgo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fkcgtsav2kgqnq61ejgtsn85fgo FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- TOC entry 2766 (class 2606 OID 34946)
-- Name: project fkeaohcvks7ix2a3vbjj9mvaghx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkeaohcvks7ix2a3vbjj9mvaghx FOREIGN KEY (type_id) REFERENCES public.project_type(id);


--
-- TOC entry 2770 (class 2606 OID 34951)
-- Name: required_skills fkfu1g6rg1luupmuuj7xnxi6dcu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fkfu1g6rg1luupmuuj7xnxi6dcu FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2767 (class 2606 OID 34956)
-- Name: proposed_skills fkhsarj7qgjgjmr5hsdlq8f38fn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fkhsarj7qgjgjmr5hsdlq8f38fn FOREIGN KEY (skill_id) REFERENCES public.skill(id);


--
-- TOC entry 2760 (class 2606 OID 34961)
-- Name: owned_companies fkil8sxyscorxq53kgc7imvho4t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.owned_companies
    ADD CONSTRAINT fkil8sxyscorxq53kgc7imvho4t FOREIGN KEY (profile_id) REFERENCES public.profile(id);


--
-- TOC entry 2761 (class 2606 OID 34966)
-- Name: owned_companies fkjgmoyc5xjswq0v4qb4ne9udf3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.owned_companies
    ADD CONSTRAINT fkjgmoyc5xjswq0v4qb4ne9udf3 FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- TOC entry 2774 (class 2606 OID 34971)
-- Name: user_role fkm90yi1ak9h9tbct25k3km0hia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkm90yi1ak9h9tbct25k3km0hia FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2772 (class 2606 OID 34976)
-- Name: statement fkosqftck2cd6g6cqjjvtx9ofho; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statement
    ADD CONSTRAINT fkosqftck2cd6g6cqjjvtx9ofho FOREIGN KEY (employee_id) REFERENCES public.company(id);


--
-- TOC entry 2773 (class 2606 OID 34981)
-- Name: statement fkpoaihwcxx9fqcnmw1ikl2ft5j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statement
    ADD CONSTRAINT fkpoaihwcxx9fqcnmw1ikl2ft5j FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2768 (class 2606 OID 34986)
-- Name: proposed_skills fksx4ibndx5e6em79nuc0nc1593; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fksx4ibndx5e6em79nuc0nc1593 FOREIGN KEY (company_id) REFERENCES public.company(id);


-- Completed on 2019-07-17 19:46:07

--
-- PostgreSQL database dump complete
--

