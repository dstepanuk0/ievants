--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-08-13 13:49:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 212 (class 1259 OID 59733)
-- Name: blocked_ips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blocked_ips (
    id bigint NOT NULL,
    address character varying(255),
    date date,
    reason character varying(255)
);


ALTER TABLE public.blocked_ips OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 51573)
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id bigint NOT NULL,
    mark_avg bigint,
    description character varying(255),
    name character varying(255) NOT NULL,
    type character varying(255),
    owner_id bigint,
    blocked boolean,
    photo_url character varying(255)
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 51645)
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 51581)
-- Name: file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file (
    id bigint NOT NULL,
    title character varying(255),
    url_file character varying(255)
);


ALTER TABLE public.file OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 51647)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 51589)
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profile (
    id bigint NOT NULL,
    accepted boolean,
    first_name character varying(25) NOT NULL,
    is_root boolean,
    last_name character varying(25) NOT NULL,
    nickname character varying(25) NOT NULL,
    photo_url character varying(255),
    company_id bigint,
    user_id bigint
);


ALTER TABLE public.profile OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 51594)
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    id bigint NOT NULL,
    budget double precision,
    description character varying(255),
    employeemark bigint,
    employermark bigint,
    expirydate date,
    name character varying(255),
    photo_url character varying(255),
    status character varying(255),
    creator_id bigint,
    employee_id bigint,
    type_id bigint
);


ALTER TABLE public.project OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 51602)
-- Name: project_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_type (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.project_type OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 51607)
-- Name: projectrequest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projectrequest (
    id bigint NOT NULL,
    employee_id bigint,
    employer_id bigint,
    project_id bigint
);


ALTER TABLE public.projectrequest OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 59731)
-- Name: projecttype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projecttype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projecttype_id_seq OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 51612)
-- Name: proposed_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proposed_skills (
    company_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.proposed_skills OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 51617)
-- Name: required_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.required_skills (
    project_id bigint NOT NULL,
    skill_id bigint NOT NULL
);


ALTER TABLE public.required_skills OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 51622)
-- Name: skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skills (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.skills OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 51649)
-- Name: skills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.skills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skills_id_seq OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 51627)
-- Name: user_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_info (
    id bigint NOT NULL,
    is_active boolean,
    password character varying(255),
    username character varying(255)
);


ALTER TABLE public.user_info OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 51635)
-- Name: user_recommendations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_recommendations (
    id bigint NOT NULL,
    valuation real,
    profile_id bigint,
    project_type_id bigint
);


ALTER TABLE public.user_recommendations OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 51640)
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    user_id bigint NOT NULL,
    roles character varying(255)
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- TOC entry 2923 (class 0 OID 59733)
-- Dependencies: 212
-- Data for Name: blocked_ips; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2907 (class 0 OID 51573)
-- Dependencies: 196
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (3, 3, '', 'Polska', 'EMPLOYEE', 14, false, NULL);
INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (2, 5, '', 'Emploee', 'EMPLOYEE', 13, false, NULL);
INSERT INTO public.companies (id, mark_avg, description, name, type, owner_id, blocked, photo_url) VALUES (1, 4, 'Goooooooood', 'Storona', 'EMPLOYER', 3, false, NULL);


--
-- TOC entry 2908 (class 0 OID 51581)
-- Dependencies: 197
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2909 (class 0 OID 51589)
-- Dependencies: 198
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.profile (id, accepted, first_name, is_root, last_name, nickname, photo_url, company_id, user_id) VALUES (3, true, 'Dmytro', NULL, 'Stepanyk', 'doromador', '', 1, 3);
INSERT INTO public.profile (id, accepted, first_name, is_root, last_name, nickname, photo_url, company_id, user_id) VALUES (14, true, 'Stepa', NULL, 'Savka', 'sava', '', 1, 14);
INSERT INTO public.profile (id, accepted, first_name, is_root, last_name, nickname, photo_url, company_id, user_id) VALUES (13, true, 'Mike', NULL, 'Palahuta', 'palah', '', 1, 13);


--
-- TOC entry 2910 (class 0 OID 51594)
-- Dependencies: 199
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (26, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Lacalut', NULL, 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (35, 200000, 'Good project', NULL, NULL, '2021-02-11', 'IDUkrain', NULL, 'FAILED', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (39, 432400, 'Your hair will be too amazing', NULL, NULL, '2020-02-29', 'Head&Sholders', NULL, 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (40, 432400, 'dgfghdhfsfgsgf tsryhrtsysryhrt', NULL, NULL, '2019-08-31', 'Adddd', NULL, 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (41, 345643, 'bbbbbbbbbbbbbbbbbbbbb', NULL, NULL, '2019-08-30', 'fjhgfty', NULL, 'NEW', 1, NULL, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (32, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Amazon', NULL, 'COMPLETED', 1, 2, 10);
INSERT INTO public.project (id, budget, description, employeemark, employermark, expirydate, name, photo_url, status, creator_id, employee_id, type_id) VALUES (29, 2000000, 'Good project', NULL, NULL, '2021-02-26', 'Bomba', NULL, 'FAILED', 1, NULL, 10);


--
-- TOC entry 2911 (class 0 OID 51602)
-- Dependencies: 200
-- Data for Name: project_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project_type (id, name) VALUES (10, 'Java');


--
-- TOC entry 2912 (class 0 OID 51607)
-- Dependencies: 201
-- Data for Name: projectrequest; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.projectrequest (id, employee_id, employer_id, project_id) VALUES (2, 2, 1, 32);


--
-- TOC entry 2913 (class 0 OID 51612)
-- Dependencies: 202
-- Data for Name: proposed_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2914 (class 0 OID 51617)
-- Dependencies: 203
-- Data for Name: required_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.required_skills (project_id, skill_id) VALUES (39, 2);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (40, 2);
INSERT INTO public.required_skills (project_id, skill_id) VALUES (41, 2);


--
-- TOC entry 2915 (class 0 OID 51622)
-- Dependencies: 204
-- Data for Name: skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.skills (id, name) VALUES (1, 'desfs');
INSERT INTO public.skills (id, name) VALUES (2, 'C++');


--
-- TOC entry 2916 (class 0 OID 51627)
-- Dependencies: 205
-- Data for Name: user_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_info (id, is_active, password, username) VALUES (1, false, '$2a$10$MZ29ZamSVrPfs4h5oGjBT.JUPmb5/EV6u5I8emSu0hSX4rnLqI/Se', 'dstepanuk0@gmail.c0m');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (2, true, '$2a$10$6lsNzPWDK4J8J2f9IBCJQOVSi53riPQDPPLGXh6Z3skVV8HgjYWN6', 'dstepanuk0@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (3, true, '$2a$10$WEBoWVlOl74k34xQUveCk.G9lwlkuif9IHDgSCxB89we76eLH3zdC', 'stepanyk@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (13, true, '$2a$10$buFjltxQ34xpamsHnAbZReqGckKUYDuc8GsNbdiSZgmIJ.ylqyf2u', 'palahuta@gmail.com');
INSERT INTO public.user_info (id, is_active, password, username) VALUES (14, true, '$2a$10$FIK/RfosPNi/Sgi8j/SRqOr032ddm.PVL0JF8e8uToxBfgsrUflgW', 'stepa@gmail.com');


--
-- TOC entry 2917 (class 0 OID 51635)
-- Dependencies: 206
-- Data for Name: user_recommendations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_recommendations (id, valuation, profile_id, project_type_id) VALUES (38, 7, 3, 10);


--
-- TOC entry 2918 (class 0 OID 51640)
-- Dependencies: 207
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.user_role (user_id, roles) VALUES (1, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (2, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (3, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (13, 'USER');
INSERT INTO public.user_role (user_id, roles) VALUES (14, 'USER');


--
-- TOC entry 2929 (class 0 OID 0)
-- Dependencies: 208
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_id_seq', 3, true);


--
-- TOC entry 2930 (class 0 OID 0)
-- Dependencies: 209
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 41, true);


--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 211
-- Name: projecttype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projecttype_id_seq', 1, false);


--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 210
-- Name: skills_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skills_id_seq', 2, true);


--
-- TOC entry 2769 (class 2606 OID 59740)
-- Name: blocked_ips blocked_ips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blocked_ips
    ADD CONSTRAINT blocked_ips_pkey PRIMARY KEY (id);


--
-- TOC entry 2745 (class 2606 OID 51580)
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- TOC entry 2747 (class 2606 OID 51588)
-- Name: file file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 51593)
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 51601)
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 51606)
-- Name: project_type project_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_type
    ADD CONSTRAINT project_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 51611)
-- Name: projectrequest projectrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT projectrequest_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 51616)
-- Name: proposed_skills proposed_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT proposed_skills_pkey PRIMARY KEY (company_id, skill_id);


--
-- TOC entry 2761 (class 2606 OID 51621)
-- Name: required_skills required_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT required_skills_pkey PRIMARY KEY (project_id, skill_id);


--
-- TOC entry 2763 (class 2606 OID 51626)
-- Name: skills skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- TOC entry 2759 (class 2606 OID 51644)
-- Name: proposed_skills uk_tnifu1r9wpm73c5gpa8nyg0oy; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT uk_tnifu1r9wpm73c5gpa8nyg0oy UNIQUE (skill_id);


--
-- TOC entry 2765 (class 2606 OID 51634)
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (id);


--
-- TOC entry 2767 (class 2606 OID 51639)
-- Name: user_recommendations user_recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT user_recommendations_pkey PRIMARY KEY (id);


--
-- TOC entry 2770 (class 2606 OID 51651)
-- Name: companies fk2rnygfpf0ca7252gep0gd0itq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT fk2rnygfpf0ca7252gep0gd0itq FOREIGN KEY (owner_id) REFERENCES public.profile(id);


--
-- TOC entry 2783 (class 2606 OID 51716)
-- Name: user_recommendations fk3ujn1vowvpmtqgx3l2y5olb1a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT fk3ujn1vowvpmtqgx3l2y5olb1a FOREIGN KEY (profile_id) REFERENCES public.profile(id);


--
-- TOC entry 2771 (class 2606 OID 51656)
-- Name: profile fk4f2icjx0bevus365xaioo14gx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk4f2icjx0bevus365xaioo14gx FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 2781 (class 2606 OID 51706)
-- Name: required_skills fk4inem3msersanj1bkgp7b1hgs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fk4inem3msersanj1bkgp7b1hgs FOREIGN KEY (skill_id) REFERENCES public.skills(id);


--
-- TOC entry 2772 (class 2606 OID 51661)
-- Name: profile fk9c6o0i2wom50761484uhrqni6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk9c6o0i2wom50761484uhrqni6 FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2777 (class 2606 OID 51686)
-- Name: projectrequest fkdofbm71dweqlrf9oq0yulpl55; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkdofbm71dweqlrf9oq0yulpl55 FOREIGN KEY (employer_id) REFERENCES public.companies(id);


--
-- TOC entry 2775 (class 2606 OID 51676)
-- Name: project fkeaohcvks7ix2a3vbjj9mvaghx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkeaohcvks7ix2a3vbjj9mvaghx FOREIGN KEY (type_id) REFERENCES public.project_type(id);


--
-- TOC entry 2782 (class 2606 OID 51711)
-- Name: required_skills fkfu1g6rg1luupmuuj7xnxi6dcu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.required_skills
    ADD CONSTRAINT fkfu1g6rg1luupmuuj7xnxi6dcu FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2774 (class 2606 OID 51671)
-- Name: project fkfwq11dxwrexdea7sg3dttyq84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkfwq11dxwrexdea7sg3dttyq84 FOREIGN KEY (employee_id) REFERENCES public.companies(id);


--
-- TOC entry 2773 (class 2606 OID 51666)
-- Name: project fkgrfaybenjkhnefl8y9sf96p4p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fkgrfaybenjkhnefl8y9sf96p4p FOREIGN KEY (creator_id) REFERENCES public.companies(id);


--
-- TOC entry 2778 (class 2606 OID 51691)
-- Name: projectrequest fkipj7o2yktf43muqeydh4vndsy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkipj7o2yktf43muqeydh4vndsy FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 2784 (class 2606 OID 51721)
-- Name: user_recommendations fkjp8ey7w6vc3jhqo1u1023pbwt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_recommendations
    ADD CONSTRAINT fkjp8ey7w6vc3jhqo1u1023pbwt FOREIGN KEY (project_type_id) REFERENCES public.project_type(id);


--
-- TOC entry 2785 (class 2606 OID 51726)
-- Name: user_role fkm90yi1ak9h9tbct25k3km0hia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkm90yi1ak9h9tbct25k3km0hia FOREIGN KEY (user_id) REFERENCES public.user_info(id);


--
-- TOC entry 2780 (class 2606 OID 51701)
-- Name: proposed_skills fknwq7xv28skpddav5kpfvyxo64; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fknwq7xv28skpddav5kpfvyxo64 FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 2779 (class 2606 OID 51696)
-- Name: proposed_skills fkoga7edw0sv4c4lwo1wpefh7l2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proposed_skills
    ADD CONSTRAINT fkoga7edw0sv4c4lwo1wpefh7l2 FOREIGN KEY (skill_id) REFERENCES public.skills(id);


--
-- TOC entry 2776 (class 2606 OID 51681)
-- Name: projectrequest fkq2xjyl0x4i374kk9d02ag4c3j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectrequest
    ADD CONSTRAINT fkq2xjyl0x4i374kk9d02ag4c3j FOREIGN KEY (employee_id) REFERENCES public.companies(id);


-- Completed on 2019-08-13 13:49:12

--
-- PostgreSQL database dump complete
--

